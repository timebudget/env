# env

This project contains files to deploy the timebudget application to kubernetes.

# Deploying with kind
Local kubernetes testing has been done with [kind](https://kind.sigs.k8s.io/).
Below docs assume you've installed kind already on your system.

## Starting the kubernetes cluster
```
kind create cluster --config cluster.yaml --name cluster
```

## Configuring the cluster with ingress-nginx
In order to support access of the application from the host, ingress-nginx can be used.

```
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/static/provider/kind/deploy.yaml
```

## Setting up custom domains
Configure the following domains in your `/etc/hosts` file to enable proper routing and local access. For more information see this [site](https://support.acquia.com/hc/en-us/articles/360004175973-Using-an-etc-hosts-file-for-custom-domains-during-development).

Configure `/etc/hosts` to assign the timebudget and timebudget-api domains to 127.0.0.1 (localhost). When complete you should have a line in `/etc/hosts` like:
```
127.0.0.1	localhost timebudget timebudget-api
```

## Deploy the application
Docker containers are currently in private registries for this project. Therefore, frst create a deploy token for the timebudget group in Gitlab. Make certain this token has read privileges for the container registry. This produces a username and password. Configure this as below:
```
kubectl create secret docker-registry gitlab-api-local-token --docker-server=registry.gitlab.com --docker-username=gitlab+deploy-token-214351 --docker-password=XXXXXXXXXXXXXXXXXXXX 
```

Deploy the application to the kubernetes cluster.
```
kubectl apply -f timebudget.yaml
```

## Enjoy
Point your local browser at http://timebudget and explore the application.

## Teardown
The kubernetes cluster can be destroyed by running the following:
```
kind delete cluster --name cluster
```

# Local testing outside of kubernetes
The following instructions assume you have all dependencies installed and can run the applications locally.

## Start the API development server
```
python -m uvicorn main:app --reload
```

## Start the client development server
```
VUE_APP_API_BASE_URL="http://localhost:8000" npm run serve
```

## Test
Navigate to http://localhost:8080 to test the application.
